﻿using System;
using System.Windows.Controls;
using System.Collections.Generic;
using SchemaEditor.Classes.Infrastructure.Tools;
using SchemaEditor.Controls;
using PhylumSilverlightClassLibrary.Classes.Model.Schema;
using PhylumSilverlightClassLibrary.Classes.Infrastructure;
using PhylumSilverlightClassLibrary.Classes.Infrastructure.Tools;
using PhylumSilverlightClassLibrary.Classes.Infrastructure.MapTools;
using PhylumSilverlightClassLibrary.Classes.Infrastructure.Data;

namespace SchemaEditor.Classes.Infrastructure
{
    public class CurrentContext : CommonContext, ICurrentContext
    {
        public override string ContextName { get { return "SchemaEditor"; } }
        // Development services
        bool _debug = false;
        public override bool isDebug { get { return _debug; } set { _debug = value; } }
        public override bool forDataAnalysis { get { return false; } }
        public MainEditorControl mainPage;

        public void addContextualTool(Type type)
        {
            base.addContextualTool(type);

            // Add tool to the toolbar
            tool.toToolbar(mainPage.mainMenu.contextualBarContainer, mainPage.mainMenu, this);

        }

        private static CurrentContext _instance;
        public static CurrentContext Instance
        {
            get
            {
                lock (typeof(CurrentContext))
                {
                    if (!isInitialized)
                        throw new Exception("Not allowed: the current context is not initialized");
                    return CurrentContext._instance;
                }
            }
            private set { }
        }

        internal static void initialize(MainEditorControl _mainPage, string token, Guid userId, bool isDebug, string username)
        {
            lock (typeof(CurrentContext))
            {
                isInitialized = true;
                new CurrentContext(_mainPage, token, userId, isDebug, username);
            }
        }

        public override void display()
        {
            base.display();
            mainPage.propertiesContainer.Content = dataManager.getRightPanelLegend();
        }

        private CurrentContext(MainEditorControl _mainPage, string thisToken, Guid userId, bool isDebug, string username)
        {
            _mainPage.ctrlStatusBar.txtDebug.Text = "";
            _instance = this;
            this.isDebug = isDebug;
            this.Username = username;

            // Initialize components and data structures
            mainPage = _mainPage;
            UiNotificationManager.initialize(new Canvas(), this, mainPage.LayoutRoot);

            generalOptions = new AGeneralOptions();
            canvasManager = new CanvasManager(mainPage.painterControl, this);

            currentSelection = new Dictionary<string, ModelObject>();
            nextSelection = new Dictionary<string, ModelObject>();
            relatedHighlight = new List<ModelObject>();

            // Create the tools for the toolbar
            fixedTools = new List<UiTool>();
            //fixedTools.Add(new ReloadTool());
            fixedTools.Add(new SelectTool(CurrentContext.Instance));
            //fixedTools.Add(new PrintTool());
            //fixedTools.Add(new SwitchboardNewTool());
            fixedTools.Add(new SchemaObjectNewTool());

            foreach (var tool in fixedTools)
                tool.toToolbar(mainPage.mainMenu.toolContainer, mainPage.mainMenu, CurrentContext.Instance);

            contextualTools = new List<UiTool>();
            // Create the contextual tools which can be called on multiple objects
            // other tools which are valid for an object only will be cached upon request
            contextualTools.Add(new SchemaObjectMoveTool(this));
            if (App.username.CompareTo("Algorab") == 0)
            {
                contextualTools.Add(new SchemaObjectCloneTool(this));
            }


            // Set the current tool
            currentTool = new SelectTool(this);

            // Get the data from the WS dynamically
            dataManager = new ADataManager(this);
            dataManager.dataStatus.token = thisToken;
            dataManager.dataStatus.userId = userId;
            dataManager.fetchAll();
            dataManager.DataProcessingCompleted += new EventHandler(dataManager_DataProcessingCompleted);

            //Initialize the main page
            mainPage.initialize();


            // Stats ticker
            renderablesCount = 0;
            ticker = new System.Windows.Threading.DispatcherTimer();
            ticker.Interval = new TimeSpan(0, 0, 0, 1, 0); // d, h, m, s, ms
            ticker.Tick += new EventHandler(ticker_Tick);
            ticker.Start();
        }

        #region Selection Handling

        public override void alterSelection(bool? isAdding, ModelObject modelObject, object sender)
        {
            CheckSelection(isAdding, modelObject, sender);

            // Clean the contextual toolbar
            mainPage.mainMenu.contextualBarContainer.Children.Clear();

            // Clean currently selected object
            mainPage.propertiesContainer.Content = null;

            if (nextSelection.Count == 1)
            {
                foreach (var nextSelectionModel in nextSelection)
                {
                    // There is only one object in here.
                    mainPage.propertiesContainer.Content = nextSelectionModel.Value.getPropertiesEditorControl();

                    // Create the contextual menu for this object
                    modelObject.createContextualMenu(mainPage.mainMenu.contextualBarContainer);
                    if (App.username.CompareTo("Algorab") == 0)
                    {
                        CurrentContext.Instance.addContextualTool(typeof(SchemaObjectCloneTool));
                    }
                }
            }
            else if (nextSelection.Count > 1)
            {
                // Multiple objects are selected
                // No initial support for this.
            }

            if (mainPage.propertiesContainer.Content == null)
            {
                mainPage.propertiesContainer.Content = dataManager.getRightPanelLegend();
            }

            // Set the new selection
            currentSelection = nextSelection;
        }


        #endregion


        public override void startSaving()
        {
            lock (isSavingMonitor)
            {
                CurrentContext.Instance.mainPage.mainBusyIndicator.IsBusy = true;
                CurrentContext.Instance.mainPage.mainBusyIndicator.BusyContent = "Salvataggio in corso...";
                base.startSaving();
            }
        }

        public override void stopSaving(bool success)
        {
            // Re-do the selection to update from DB and cleanup
            SetStopSaving(success);

            CurrentContext.Instance.mainPage.mainBusyIndicator.IsBusy = false;
        }
    }
}
