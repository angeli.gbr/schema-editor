﻿using System.Windows;
using PhylumSilverlightClassLibrary.Classes.Infrastructure.Tools;
using PhylumSilverlightClassLibrary.Classes.Model.Schema;
using PhylumSilverlightClassLibrary.Classes.Model.View;


namespace SchemaEditor.Classes.Infrastructure.Tools
{
    public class SchemaObjectNewTool : UiTool {
      public override string toolbarButtonImage { get { return "bullet_add"; } set { } }
      public override string toolbarButtonTooltip { get { return "Crea oggetto"; } set { } }

    public SchemaObjectNewTool()
    {
    }

    // Click on map -> move & do NOT change current association
    // If associated, add the projection connector
    public override void clickOnCanvas(Point xy) {
      SchemaObjectModel schemaObject = new SchemaObjectModel(CurrentContext.Instance);

      schemaObject.isReady = false;
      ((SchemaObjectView)schemaObject.view).realAnchor = xy;
      schemaObject.isReady = true;
      
      // Select it
      CurrentContext.Instance.setSelection(schemaObject, this);

      // Open its properties
      schemaObject.editProperties();
      
      // Set the tool to Select
      CurrentContext.Instance.currentTool = new SelectTool(CurrentContext.Instance);
    }
  }
}
