﻿using System;
using System.IO;
using System.Json;
using System.Net;
using System.Windows;
using C1.Silverlight;
using PhylumSilverlightClassLibrary.Classes.Infrastructure;
using PhylumSilverlightClassLibrary.Classes.Infrastructure.MapTools;
using PhylumSilverlightClassLibrary.Classes.Infrastructure.Tools;
using SchemaEditor.Controls;

namespace SchemaEditor.Classes.Infrastructure.Tools
{
    public class SchemaObjectCloneTool : SchemaObjectMoveTool
    {
        public override string toolbarButtonImage { get { return "duplicate"; } set { } }
        public override string toolbarButtonTooltip { get { return "Duplica oggetto"; } set { } }


        public SchemaObjectCloneTool(ICurrentContext context)
                : base(context)
        {
            
        }

        public override bool activation(bool explicitly)
        {
            int countDirtyObjects = CurrentContext.Instance.dataManager.dirtyObjects.Count;
            if (countDirtyObjects > 0)
            {
                C1MessageBox.Show("Attenzione, dal momento che sono presenti delle modifiche allo schema è necessario procedere al salvataggio dello schema prima di poter continuare con l'operazione.", "Attenzione", C1MessageBoxButton.OK, C1MessageBoxIcon.Warning);
                context.currentTool = new SelectTool(context);
                return false;
            }
            else
            {
                var schemaObject = getSelectedSchemaObject();
                if (schemaObject == null)
                {
                    C1MessageBox.Show("Attenzione, è necessario selezionare un oggetto da duplicare prima di poter procedere con l'operazione.", "Attenzione", C1MessageBoxButton.OK, C1MessageBoxIcon.Warning);
                    context.currentTool = new SelectTool(context);
                    return false;
                }
                if (schemaObject.category != PhylumSilverlightClassLibrary.DataService.SvcSchemaObjectCategory.General)
                {
                    C1MessageBox.Show("Attenzione, non è possibile duplicare oggetti con categoria di tipo Risorsa, Parametro oppure Sistema.", "Attenzione", C1MessageBoxButton.OK, C1MessageBoxIcon.Warning);
                    context.currentTool = new SelectTool(context);
                    return false;
                }
                C1MessageBox.Show("Selezionare un punto dello schema che verrà utilizzato per posizionare il nuovo oggetto.", "Informazione", C1MessageBoxButton.OK, C1MessageBoxIcon.Information);
                return true;
            }
        }

        // Click on map -> move & do NOT change current association
        // If associated, add the projection connector
        public override void clickOnCanvas(Point xy)
        {
            var schemaObject = getSelectedSchemaObject();
            if (schemaObject == null)
                return;

            schemaObjectFromName = schemaObject.name;
            x = Convert.ToInt32(xy.X);
            y = Convert.ToInt32(xy.Y);
            InputPromptDialog dlg = new InputPromptDialog("Duplicazione oggetto",
                    "Nome del nuovo oggetto?",
                    HandlePrompt);

            // Set the tool to Select
            context.currentTool = new SelectTool(context);
        }

        string schemaObjectFromName;
        int x;
        int y;
        private void HandlePrompt(string input)
        {
            CurrentContext.Instance.mainPage.mainBusyIndicator.BusyContent = "Duplicazione oggetto in corso...";
            CurrentContext.Instance.mainPage.mainBusyIndicator.IsBusy = true;
            beginRequest(input);
        }
        private string postData;
        public void beginRequest(string schemaObjectToName)
        {
            try
            {
                // Assemble postData string
                postData = "schemaObjectFromName=" + Uri.EscapeUriString(schemaObjectFromName) + "&schemaObjectToName=" + Uri.EscapeUriString(schemaObjectToName) + "&x=" + x + "&y=" + y + "&token=" + Uri.EscapeUriString(CurrentContext.Instance.dataManager.dataStatus.token);
                var absPath = Application.Current.Host.Source.AbsolutePath;
                var appPath = absPath.Substring(0, absPath.IndexOf("/ClientBin/"));
                // Set destination url
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(Application.Current.Host.Source.Scheme + "://" + Application.Current.Host.Source.Host +
                                  ":" + Application.Current.Host.Source.Port + appPath + "/UniverseManagement/DuplicateSchemaObjectFromSilverlight"));
                // Set request method, using POST of course
                request.Method = "POST";
                // This is important
                request.ContentType = "application/x-www-form-urlencoded";
                // Start request
                request.BeginGetRequestStream(new AsyncCallback(RequestReady), request);
            }
            catch (Exception e)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    CurrentContext.Instance.mainPage.mainBusyIndicator.IsBusy = false;
                    C1MessageBox.Show("Attenzione, duplicazione oggetto non effettuata correttamente!\nErrore: " + e.Message, "Errore", C1MessageBoxIcon.Error);
                });
            }
        }
        void RequestReady(IAsyncResult asyncResult)
        {
            try
            {
                HttpWebRequest request = asyncResult.AsyncState as HttpWebRequest;
                // Retrieve created stream
                Stream stream = request.EndGetRequestStream(asyncResult);
                StreamWriter writer = new StreamWriter(stream);
                // Write data to the stream
                writer.Write(postData);
                writer.Flush();
                writer.Close();
                // Expect response
                request.BeginGetResponse(new AsyncCallback(ResponseReady), request);
            }
            catch (Exception e)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    CurrentContext.Instance.mainPage.mainBusyIndicator.IsBusy = false;
                    C1MessageBox.Show("Attenzione, duplicazione oggetto non effettuata correttamente!\nErrore: " + e.Message, "Errore", C1MessageBoxIcon.Error);
                });
            }
        }
        // Called when the response is ready
        void ResponseReady(IAsyncResult asyncResult)
        {
            try
            {
                HttpWebRequest request = asyncResult.AsyncState as HttpWebRequest;
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asyncResult);

                // Retrieve response stream
                Stream responseStream = response.GetResponseStream();
                JsonObject result = (JsonObject)JsonObject.Load(responseStream);
                bool success = result["success"];
                if (success)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        CurrentContext.Instance.mainPage.mainBusyIndicator.IsBusy = false;
                        C1MessageBox.Show("Duplicazione oggetto effettuata correttamente!", "Avviso", C1MessageBoxIcon.Information, ReloadData);
                    });
                }
                else
                {
                    string message = result["message"];
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        CurrentContext.Instance.mainPage.mainBusyIndicator.IsBusy = false;
                        C1MessageBox.Show("Attenzione, duplicazione oggetto non effettuata correttamente!\nErrore: " + message, "Errore", C1MessageBoxIcon.Error);
                    });
                }
            }
            catch (Exception e)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    CurrentContext.Instance.mainPage.mainBusyIndicator.IsBusy = false;
                    C1MessageBox.Show("Attenzione, gruppo di punti luce per la gestione delle regole di illuminazione non creato correttamente!\nErrore: " + e.Message, "Errore", C1MessageBoxIcon.Error);
                });
            }
        }

        private void ReloadData(MessageBoxResult res)
        {
            // Call JS to reload
            System.Windows.Browser.HtmlPage.Window.Eval("window.location.reload();");
        }
    }
}
