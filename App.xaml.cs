﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SchemaEditor.Controls;
using SchemaEditor.Classes.Infrastructure;
using System.Windows.Browser;
using PhylumSilverlightClassLibrary.Classes.Util;

namespace SchemaEditor {
  public partial class App : Application {

     public static string username = "";
     private BusyIndicator progressIndicator;
     SaveManager saveManager;

    public App() {
      this.Startup += this.Application_Startup;
      this.Exit += this.Application_Exit;
      this.UnhandledException += this.Application_UnhandledException;

      InitializeComponent();
    }

    private void Application_Startup(object sender, StartupEventArgs e) {

      string token = "";
      //string username = "";
      Guid userId = Guid.Empty;
      bool isDebug = false;
      var paramValues = e.InitParams;
      foreach (var p in paramValues) {
        if (p.Key == "token")
          token = p.Value;
        else if (p.Key == "awots" && p.Value == "1") {
          isDebug = true;
        }
        else if (p.Key == "username")
        {
            username = p.Value;
        }
        else if (p.Key == "userId")
            Guid.TryParse(p.Value, out userId);
      }

      this.progressIndicator = new BusyIndicator();
      this.Resources.Add("MainProgress", this.progressIndicator);
      this.progressIndicator.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.progressIndicator.VerticalAlignment = VerticalAlignment.Stretch;
      this.progressIndicator.BusyContent = "Caricamento dei dati in corso...";
      this.progressIndicator.Content = new MainEditorControl();
      this.RootVisual = this.progressIndicator;
      //this.RootVisual = new MainEditorControl();

      //CurrentContext.initialize((MainEditorControl)this.RootVisual, token, isDebug, username);
      CurrentContext.initialize((MainEditorControl)this.progressIndicator.Content, token, userId, isDebug, username);

      saveManager = new SaveManager(CurrentContext.Instance);
      HtmlPage.RegisterScriptableObject("SaveManager", saveManager);
    }

    private void Application_Exit(object sender, EventArgs e) {

    }

    private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e) {
      // If the app is running outside of the debugger then report the exception using
      // the browser's exception mechanism. On IE this will display it a yellow alert 
      // icon in the status bar and Firefox will display a script error.
      if (!System.Diagnostics.Debugger.IsAttached) {

        // NOTE: This will allow the application to continue running after an exception has been thrown
        // but not handled. 
        // For production applications this error handling should be replaced with something that will 
        // report the error to the website and stop the application.
        e.Handled = true;
        Deployment.Current.Dispatcher.BeginInvoke(delegate { ReportErrorToDOM(e); });

        CurrentContext.Instance.mainPage.ctrlStatusBar.txtDebug.Text = e.ExceptionObject.Message;
      }
    }

    private void ReportErrorToDOM(ApplicationUnhandledExceptionEventArgs e) {
      try {
        string errorMsg = e.ExceptionObject.Message + e.ExceptionObject.StackTrace;
        errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");

        HtmlPage.Window.Eval("throw new Error(\"Unhandled Error in Silverlight Application " + errorMsg + "\");");
      }
      catch (Exception) {
      }
    }
  }
}
