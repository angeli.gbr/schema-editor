﻿using System;
using System.Windows.Controls;
using System.Windows.Threading;
using PhylumSilverlightClassLibrary.DataService;

namespace SchemaEditor
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();

            SchemaServiceClient sc = new SchemaServiceClient(/*endpointName*/);
            sc.GetServerTimeCompleted += (s, e) =>
            {
                txtServerDate.Text = e.Result.ToLongTimeString();
            };

            DispatcherTimer dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromSeconds(1);
            dt.Tick += (s, e) =>
            {
                sc.GetServerTimeAsync();
            };
            dt.Start();

            sc.GetSchemaCompleted += (s, e) =>
            {
                txtSchemaVersion.Text = "Schema versione " + e.Result.version;
            };
            //sc.GetSchemaAsync();
        }
    }
}
