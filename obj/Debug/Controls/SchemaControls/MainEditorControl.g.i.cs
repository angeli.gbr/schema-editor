﻿#pragma checksum "C:\Users\gabriele.angeli\Desktop\Augeg4.web\phylum\SchemaEditor\Controls\SchemaControls\MainEditorControl.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D8515D270690D56329CF7D0FC83A471C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CollapsingGridSplitter.Controls;
using PhylumSilverlightClassLibrary.Controls;
using SchemaEditor.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace SchemaEditor.Controls {
    
    
    public partial class MainEditorControl : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Border bodyContainer;
        
        internal SchemaEditor.Controls.MenuControl mainMenu;
        
        internal System.Windows.Controls.Border bdrSchemaContainer;
        
        internal System.Windows.Controls.Border bdrPainterControl2;
        
        internal PhylumSilverlightClassLibrary.Controls.SchemaPainterControl painterControl;
        
        internal CollapsingGridSplitter.Controls.ExtendedGridSplitter rightColGridSplitter;
        
        internal System.Windows.Controls.Border bdrPropertiesContainer;
        
        internal System.Windows.Controls.ScrollViewer propertiesContainer;
        
        internal System.Windows.Controls.BusyIndicator mainBusyIndicator;
        
        internal PhylumSilverlightClassLibrary.Controls.StatusBarControl ctrlStatusBar;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/SchemaEditor;component/Controls/SchemaControls/MainEditorControl.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.bodyContainer = ((System.Windows.Controls.Border)(this.FindName("bodyContainer")));
            this.mainMenu = ((SchemaEditor.Controls.MenuControl)(this.FindName("mainMenu")));
            this.bdrSchemaContainer = ((System.Windows.Controls.Border)(this.FindName("bdrSchemaContainer")));
            this.bdrPainterControl2 = ((System.Windows.Controls.Border)(this.FindName("bdrPainterControl2")));
            this.painterControl = ((PhylumSilverlightClassLibrary.Controls.SchemaPainterControl)(this.FindName("painterControl")));
            this.rightColGridSplitter = ((CollapsingGridSplitter.Controls.ExtendedGridSplitter)(this.FindName("rightColGridSplitter")));
            this.bdrPropertiesContainer = ((System.Windows.Controls.Border)(this.FindName("bdrPropertiesContainer")));
            this.propertiesContainer = ((System.Windows.Controls.ScrollViewer)(this.FindName("propertiesContainer")));
            this.mainBusyIndicator = ((System.Windows.Controls.BusyIndicator)(this.FindName("mainBusyIndicator")));
            this.ctrlStatusBar = ((PhylumSilverlightClassLibrary.Controls.StatusBarControl)(this.FindName("ctrlStatusBar")));
        }
    }
}

