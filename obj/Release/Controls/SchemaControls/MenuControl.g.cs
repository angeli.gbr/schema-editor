﻿#pragma checksum "D:\Repos\algorab-developer\ABU_DHABI_REBASE\AEC-Abu Dhabi\augeG4\phylum\SchemaEditor\Controls\SchemaControls\MenuControl.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "A35758EC2EA61A565F7FEFCCE51ED893"
//------------------------------------------------------------------------------
// <auto-generated>
//     Il codice è stato generato da uno strumento.
//     Versione runtime:4.0.30319.42000
//
//     Le modifiche apportate a questo file possono provocare un comportamento non corretto e andranno perse se
//     il codice viene rigenerato.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace SchemaEditor.Controls {
    
    
    public partial class MenuControl : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Button buttonSave;
        
        internal System.Windows.Controls.Button buttonSystemFieldEdit;
        
        internal System.Windows.Controls.StackPanel toolContainer;
        
        internal System.Windows.Controls.Border contextualBarHolder;
        
        internal System.Windows.Controls.StackPanel contextualBarContainer;
        
        internal System.Windows.Controls.Border toolOptionsHolder;
        
        internal System.Windows.Controls.StackPanel toolOptionsContainer;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/SchemaEditor;component/Controls/SchemaControls/MenuControl.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.buttonSave = ((System.Windows.Controls.Button)(this.FindName("buttonSave")));
            this.buttonSystemFieldEdit = ((System.Windows.Controls.Button)(this.FindName("buttonSystemFieldEdit")));
            this.toolContainer = ((System.Windows.Controls.StackPanel)(this.FindName("toolContainer")));
            this.contextualBarHolder = ((System.Windows.Controls.Border)(this.FindName("contextualBarHolder")));
            this.contextualBarContainer = ((System.Windows.Controls.StackPanel)(this.FindName("contextualBarContainer")));
            this.toolOptionsHolder = ((System.Windows.Controls.Border)(this.FindName("toolOptionsHolder")));
            this.toolOptionsContainer = ((System.Windows.Controls.StackPanel)(this.FindName("toolOptionsContainer")));
        }
    }
}

