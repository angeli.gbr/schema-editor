﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.ComponentModel;
using SchemaEditor.Classes.Infrastructure;
using PhylumSilverlightClassLibrary.Classes.Model.Schema;
using PhylumSilverlightClassLibrary.DataService;
using PhylumSilverlightClassLibrary.Controls;

namespace SchemaEditor.Controls
{
    public partial class EditSystemFieldsControl : UserControl
    {  
        public Guid selectedObj;
        public string selectedFieldName;
        public SchemaObjectField sysField;
        public List<EditSystemFieldItem> itemObjects;
       
        private Dictionary<string, SchemaObjectField> tempSysFields;
        private Dictionary<Guid, SchemaObjectModel> tempobjects;
        private MenuControl menu;
        private bool isSetFieldFirst = false;

        public EditSystemFieldsControl(MenuControl menu)
        {
            this.menu = menu;



            InitializeComponent();
           


            #region copy SchemaObjectModel
            /*Copia degli SchemaObjectModel*/
            tempSysFields = new Dictionary<string, SchemaObjectField>();
            tempobjects = new Dictionary<Guid, SchemaObjectModel>();
            SchemaObjectModel tempObj;
            SchemaObjectField tempField;
            foreach (var o in CurrentContext.Instance.dataManager.allSchemaObjects)
            {
                if (o.op != SvcOpType.delete)
                {
                    tempObj = new SchemaObjectModel(CurrentContext.Instance);
                    tempObj.isReady = false;
                    tempObj.id = o.id;
                    tempObj.name = o.name;
                    tempObj.viewGroups.Clear();
                    foreach (var g in o.viewGroups)
                    {
                        tempObj.viewGroups.Add(g);
                    }

                    foreach (var f in o.fields)
                    {
                        if ((f.Value.type == SvcFieldType.FieldChoice &&
                            f.Value.lookup == null &&
                            f.Value.metadata != null &&
                            f.Value.metadata.Contains("_")) ||
                            (f.Value.type == SvcFieldType.FieldChoice &&
                            f.Value.lookup != null &&
                            f.Value.lookup.mode == SvcFieldLookupMode.ReferencedFields))
                        {
                            //funzionalità non gestita per il momento,
                            //quando si farà andranno copiati anche i campi relazionati
                        }
                        else
                        {
                            tempField = f.Value.dump();
                            if (!o.isDirty())
                                tempField.op = SvcOpType.noop;

                            if (!tempSysFields.ContainsKey(f.Value.name))
                            {
                                tempField.isGlobalEdit = true;
                                tempSysFields.Add(f.Value.name, tempField);
                            }

                            tempObj.fields.Add(f.Key, tempField);
                        }
                    }

                    tempobjects.Add(o.id, tempObj);
                }

            }
            #endregion


            tempSysFields = (from pair in tempSysFields orderby pair.Key select pair).ToDictionary(pair => pair.Key, pair => pair.Value);
            


            itemsSysFieldControl.ItemsSource = tempSysFields;

            chkEditAllObject.IsEnabled = false;


            

            // Editing fields
            btnEditField.Click += (s, e) => {
                selectedField();
            };
            btnAddField.Click += (s, e) => {
                if(tempobjects.Count > 0 )
                {
                    SchemaObjectField tmpField = new SchemaObjectField(tempobjects.Select(x => x.Value).First(), CurrentContext.Instance);
                    //tmpField.isSystemField = true;
                    tmpField.isGlobalEdit = true;
                    selected(tmpField);
                }
            };


            btnSave.Click += new RoutedEventHandler(btnSave_Click);
            btnCancel.Click += new RoutedEventHandler(btnCancel_Click);
            
        
        }


        private void tempObjSetField_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            itemsObjectControl.SelectedItem = sender;

            if (e.PropertyName.CompareTo("edit") == 0)
            {
                if (((EditSystemFieldItem)itemsObjectControl.SelectedItem).edit)
                {
                    chkEditAllObject.IsChecked = true;
                    bool hasAlreadyField = false;
                    foreach (var f in tempobjects[((EditSystemFieldItem)itemsObjectControl.SelectedItem).id].fields)
                    {
                        if (f.Value.name.CompareTo(selectedFieldName) == 0)
                        {
                            tempobjects[((EditSystemFieldItem)itemsObjectControl.SelectedItem).id].fields[f.Key].op = SvcOpType.update;
                            hasAlreadyField = true;
                            break;
                        }
                    }

                    if (!hasAlreadyField)
                    {
                        SchemaObjectField tmpField = tempSysFields[selectedFieldName].dump();
                        tmpField.schemaObject = tempobjects[((EditSystemFieldItem)itemsObjectControl.SelectedItem).id];
                        tmpField.id = Guid.NewGuid();
                        tmpField.op = SvcOpType.update;
                        tmpField.isNew = true;
                        tempobjects[((EditSystemFieldItem)itemsObjectControl.SelectedItem).id].addField(tmpField);
                    }
                }
                else
                {
                    chkEditAllObject.IsChecked = (from o in itemObjects where o.edit && o.editEnabled select o).Count() == 0 ? false : true;
                    foreach(var f in tempobjects[((EditSystemFieldItem)itemsObjectControl.SelectedItem).id].fields)
                    {
                        if (f.Value.name.CompareTo(selectedFieldName) == 0)
                        {
                            tempobjects[((EditSystemFieldItem)itemsObjectControl.SelectedItem).id].fields[f.Key].op = SvcOpType.delete;
                            break;
                        }
                    }
                }
            }
        }
        

        public void refreshDisplayData()
        {
            itemObjects = new List<EditSystemFieldItem>();
            List<SchemaObjectModel> tempObjectModel = (from c in tempobjects orderby c.Value.name select c.Value).ToList();
            EditSystemFieldItem tempObjSetField;
            foreach (var o in tempObjectModel)
            {
                

                    tempObjSetField = new EditSystemFieldItem();
                    tempObjSetField.id = o.id;
                    tempObjSetField.name = o.name;

                    SchemaObjectField selectedFields = null;
                    try
                    {
                        selectedFields = (from f in o.fields where ((f.Value.name.CompareTo(selectedFieldName)) == 0) select f.Value).First();
                        tempObjSetField.edit = selectedFields != null && selectedFields.op != SvcOpType.delete;
                        tempObjSetField.editEnabled = (selectedFields.tag == null || selectedFields.tag.Trim().CompareTo("") == 0);
                    }
                    catch (Exception) {
                        tempObjSetField.edit = false;
                        tempObjSetField.editEnabled = true;
                    }
                    
                   
                    //if (tempSysFields[selectedFieldName].op == DataService.SvcOpType.delete)
                    //    tempObjSetField.editEnabled = false;
                    //else
                    //    tempObjSetField.editEnabled = true;

                    tempObjSetField.PropertyChanged += new PropertyChangedEventHandler(tempObjSetField_PropertyChanged);
                    itemObjects.Add(tempObjSetField);
                
            }   
        }

      


        private void itemsSysFieldControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                selectedFieldName = ((KeyValuePair<string, SchemaObjectField>)(((ListBox)sender).SelectedItem)).Key;
                refreshDisplayData();
                itemsObjectControl.ItemsSource = itemObjects;

                chkEditAllObject.IsChecked = (from o in itemObjects where o.edit && o.editEnabled select o).Count() == 0 ? false : true;
 
               
                chkEditAllObject.IsEnabled = true;
            }
            catch (Exception) { chkEditAllObject.IsEnabled = false; }
                
            
        }


        private void savePermanently()
        {
            foreach (var o in tempobjects)
            {
                var sc = CurrentContext.Instance.dataManager.FindSchemaObject(o.Key);
                foreach (var f in o.Value.fields)
                {
                    switch(f.Value.op)
                    {
                        case SvcOpType.add:
                            //add 
                            if (!sc.fields.ContainsKey(f.Value.id))
                            {
                                f.Value.isGlobalEdit = false;
                                f.Value.op = SvcOpType.update;
                                sc.addField(f.Value);
                                f.Value.isNew = false;
                                sc.moveFieldUp(false, new KeyValuePair<Guid, SchemaObjectField>(f.Value.id, f.Value));
                                sc.resetDataEditor();
                            }
                            break;
                        case SvcOpType.update:
                            if (sc.fields.ContainsKey(f.Value.id))
                            {  //update

                                sc.fields[f.Value.id].restore(f.Value);
                                sc.fields[f.Value.id].isGlobalEdit = false;
                                sc.resetDataEditor();
                                sc.markDirty(CurrentContext.Instance);
                            }
                            else
                            {   //add 
                                f.Value.isGlobalEdit = false;
                                sc.addField(f.Value);
                                f.Value.isNew = false;
                                sc.moveFieldUp(false, new KeyValuePair<Guid, SchemaObjectField>(f.Value.id, f.Value));
                                sc.resetDataEditor(); 
                            }
                            break;
                        case SvcOpType.delete:
                            if (sc.fields.ContainsKey(f.Value.id))
                            {
                                sc.fields[f.Value.id].op = SvcOpType.delete;
                                sc.fields[f.Value.id].isGlobalEdit = false;
                                sc.resetDataEditor();
                                sc.markDirty(CurrentContext.Instance);
                            }
                            break;
                    }
                }
            }
        }


        void btnSave_Click(object sender, RoutedEventArgs e)
        {
           
            savePermanently();

            this.menu.popupSystemFields.Close();
            this.menu.popupSystemFields = null;
        }


        void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.menu.popupSystemFields.Close();
            this.menu.popupSystemFields = null;
        }


        public void selectedField()
        {
            if (itemsSysFieldControl.SelectedItem != null)
                selected(getSelectedField().Value);
        }


        private KeyValuePair<string, SchemaObjectField> getSelectedField()
        {
            return (KeyValuePair<string, SchemaObjectField>)itemsSysFieldControl.SelectedItem;
        }

        public ChildWindow popup;
        internal void selected(SchemaObjectField field)
        {
            bool isNew = field.isNew;
           
            popup = new ChildWindow()
            {
                Title = "Gestione avanzata campi",
                Width = 400,
            };

            StackPanel sp = new StackPanel()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
                VerticalAlignment = System.Windows.VerticalAlignment.Top,
            };

            var editor = new FieldEditMainControl(field, true, CurrentContext.Instance, true);
            sp.Children.Add(editor);

            popup.Closed += (s, e) =>
            {
                if (editor.getIsSaved())
                {
                    bool mustBeSaved = true;
                    foreach (var f in tempSysFields)
                    {
                        if (f.Value.id != field.id)
                        {
                            if (field.name.Trim().CompareTo(f.Value.name.Trim()) == 0)
                            {
                                mustBeSaved = false;
                                break;
                            }
                        }
                    }

                    if (mustBeSaved)
                    {
                        if (isNew)
                        {
                            foreach (var o in tempobjects)
                            {
                                SchemaObjectField tmpField = field.dump();
                                tmpField.schemaObject = o.Value;
                                tmpField.id = Guid.NewGuid();
                                tmpField.isNew = false;
                                tmpField.viewGroups.Clear();
                                foreach (var g in o.Value.viewGroups)
                                {
                                    tmpField.viewGroups.Add(g);
                                }
                                tmpField.editGroups.Clear();
                                if (!tmpField.isSystemField)
                                {
                                    foreach (var g in o.Value.editGroups)
                                    {
                                        tmpField.editGroups.Add(g);
                                    }
                                }

                                
                                o.Value.addField(tmpField);
                            }

                            field.isNew = false;
                            tempSysFields.Add(field.name, field);

                            selectedFieldName = field.name;

                            itemsSysFieldControl.ItemsSource = null;
                            itemsSysFieldControl.ItemsSource = tempSysFields;
                            itemsSysFieldControl.Focus();
                           

                            if (itemsSysFieldControl != null && itemsSysFieldControl.Items != null && itemsSysFieldControl.Items.Count > 0)
                            {
                                int index = 0;
                                foreach (KeyValuePair<string, SchemaObjectField> i in itemsSysFieldControl.Items)
                                {
                                    if (i.Key.CompareTo(selectedFieldName) == 0)
                                        break;
                                    index++;
                                }


                                itemsSysFieldControl.SelectedIndex = index;
                            }

                           

                        }
                        else
                        {

                            foreach (var o in tempobjects)
                            {
                                foreach (var f in o.Value.fields)
                                {
                                    if (f.Value.name.CompareTo(selectedFieldName) == 0)
                                    {
                                        Guid id = f.Value.id;
                                        f.Value.restore(field);
                                        f.Value.id = id;
                                        if (f.Value.isSystemField)
                                        {
                                            f.Value.editGroups.Clear();
                                        }
                                    }
                                }
                            }

                            tempSysFields.Remove(selectedFieldName);
                            tempSysFields.Add(field.name, field);

                            selectedFieldName = field.name;

                            itemsSysFieldControl.ItemsSource = null;
                            itemsSysFieldControl.ItemsSource = tempSysFields;
                            itemsSysFieldControl.Focus();


                            if (itemsSysFieldControl != null && itemsSysFieldControl.Items != null && itemsSysFieldControl.Items.Count > 0)
                            {
                                int index = 0;
                                foreach (KeyValuePair<string, SchemaObjectField> i in itemsSysFieldControl.Items)
                                {
                                    if (i.Key.CompareTo(selectedFieldName) == 0)
                                        break;
                                    index++;
                                }


                                itemsSysFieldControl.SelectedIndex = index;
                            }
                        }
                    }
                }

            };

            popup.Content = sp;
            popup.Show();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            setAllChecks(((CheckBox)sender).IsChecked.Value);
        }



        private void setAllChecks(bool value)
        {
            if (value)
            {
                foreach (var o in itemObjects)
                {
                    if (!o.edit)
                    {
                        o.edit = true;
                        bool hasAlreadyField = false;
                        foreach (var f in tempobjects[o.id].fields)
                        {
                            if (f.Value.name.CompareTo(selectedFieldName) == 0)
                            {
                                f.Value.op = SvcOpType.update;
                                hasAlreadyField = true;
                                break;
                            }
                        }

                        if (!hasAlreadyField)
                        {
                            SchemaObjectField tmpField = tempSysFields[selectedFieldName].dump();
                            tmpField.schemaObject = tempobjects[o.id];
                            tmpField.id = Guid.NewGuid();
                            tmpField.op = SvcOpType.update;
                            tmpField.isNew = true;

                            tempobjects[o.id].addField(tmpField);
                        }

                    }
                }
            }
            else
            {
                foreach (var o in itemObjects)
                {
                    if (o.edit)
                    {
                        foreach (var f in tempobjects[o.id].fields)
                        {
                            if (f.Value.name.CompareTo(selectedFieldName) == 0 && o.editEnabled)
                            {
                                o.edit = false;
                                tempobjects[o.id].fields[f.Key].op = SvcOpType.delete;
                                break;
                            }
                        }
                    }
                }
            }
        }


    }




    public class EditSystemFieldItem : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        public Guid id { get; set; }
        public string name { get; set; }
       
        private bool _edit;
        public bool edit
        {
            get { return _edit; }
            set
            {
                _edit = value;
                NotifyPropertyChanged("edit");
            }
        }

        private bool _editEnabled;
        public bool editEnabled
        {
            get { return _editEnabled; }
            set
            {
                _editEnabled = value;
                NotifyPropertyChanged("editEnabled");
            }
        }


        public EditSystemFieldItem()
        {
            id = Guid.NewGuid();
            _edit = true;
            _editEnabled = true;
        }
    }


   
}
