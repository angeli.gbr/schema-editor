﻿using System.Windows;
using System.Windows.Controls;
using SchemaEditor.Classes.Infrastructure;

namespace SchemaEditor.Controls
{
    public partial class MenuControl : UserControl
    {
        BusyIndicator busy;

        public MenuControl()
        {
            // Required to initialize variables
            InitializeComponent();

            ToolTipService.SetToolTip(buttonSave, "Salva");
            ToolTipService.SetToolTip(buttonSystemFieldEdit, "Gestione avanzata campi");

            if (App.username.CompareTo("Algorab") == 0)
                buttonSystemFieldEdit.Visibility = System.Windows.Visibility.Visible;
            else
                buttonSystemFieldEdit.Visibility = System.Windows.Visibility.Collapsed;

            buttonSave.Click += (s, e) =>
            {
                CurrentContext.Instance.dataManager.saveCommand();          
            };

            buttonSystemFieldEdit.Click += (s, e) =>
            {
                
                openModifySystemFields();
            };


            
        }

        public ChildWindow popupSystemFields;
        internal void openModifySystemFields()
        {
            busy = (BusyIndicator)App.Current.RootVisual;
            busy.IsBusy = true;
            Application.Current.RootVisual.SetValue(Control.IsEnabledProperty, false); 

            popupSystemFields = new ChildWindow()
            {
                Title = "Gestione avanzata campi",
                Width = 700,
            };

            StackPanel sp = new StackPanel()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Top,
            };

            CurrentContext.Instance.setSelection(null, CurrentContext.Instance);

            var editor = new EditSystemFieldsControl(this);
            sp.Children.Add(editor);
        
            popupSystemFields.Content = sp;
            popupSystemFields.Show();
            popupSystemFields.Closed += (s, e) =>
            {
                busy.IsBusy = false;
                Application.Current.RootVisual.SetValue(Control.IsEnabledProperty, true); 
            };
        }

    }
}