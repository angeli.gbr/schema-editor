﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SchemaEditor.Classes.Infrastructure;

namespace SchemaEditor.Controls {
  public partial class MainEditorControl : UserControl {
    public MainEditorControl() {
      InitializeComponent();
    }

    public void initialize() {
      LayoutRoot.DataContext = CurrentContext.Instance.generalOptions;
    }
  }
}
