﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SchemaEditor.Controls
{
    public partial class InputPromptDialog : UserControl
	{
        private Action<string> callback;
        private ChildWindow popup;

		public InputPromptDialog(string title, string messageText, Action<string> callback)
		{
            popup = new ChildWindow()
            {
                Title = title
            };

            this.callback = callback;
			// Richiesto per inizializzare le variabili
			InitializeComponent();
            textBlock.Text = messageText; 

            popup.Content = this;
            popup.Show();
		}

        public string getInputText()
        {
            return textBox.Text;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            callback(getInputText());
            popup.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            popup.Close();
        }
	}
}